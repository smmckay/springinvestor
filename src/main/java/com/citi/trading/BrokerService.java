package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("accounts")
public class BrokerService {
	
	private Map<Integer,Investor> map = new HashMap<> ();
	
	@Autowired
	private ApplicationContext context;

	@PostConstruct
	public void init() {
		
		Investor investor1 =  context.getBean(Investor.class);
		investor1.setID();
		investor1.setCash(10000);
		Map<String,Integer> starter1 = new HashMap<>();
		starter1.put("MSFT", 100);
		investor1.setPortfolio(starter1);
	
		Investor investor2 =  context.getBean(Investor.class);
		investor2.setID();
		investor2.setCash(20000);
		Map<String,Integer> starter2 = new HashMap<>();
		starter2.put("GOOG", 200);
		investor2.setPortfolio(starter2);
		
		Investor investor3 =  context.getBean(Investor.class);
		investor3.setID();
		investor3.setCash(30000);
		Map<String,Integer> starter3 = new HashMap<>();
		starter3.put("MRK", 300);
		investor3.setPortfolio(starter3);
		
		Investor investor4 =  context.getBean(Investor.class);
		investor4.setID();
		investor4.setCash(40000);
		Map<String,Integer> starter4 = new HashMap<>();
		starter4.put("APP", 400);
		investor4.setPortfolio(starter4);
		
		map.put(investor1.getID(),investor1);
		map.put(investor2.getID(),investor2);
		map.put(investor3.getID(),investor3);
		map.put(investor4.getID(),investor4);


	}
	@RequestMapping(method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Investor> getAll(){

        List<Investor> result = new ArrayList<Investor> ();
        result.addAll (map.values ());
        return result;
	}
    @RequestMapping(value="/{ID}",method=RequestMethod.GET)
	public ResponseEntity<Investor> getByID(@PathVariable("ID") int ID) {
    	if (!map.containsKey(ID)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Investor>(map.get(ID), HttpStatus.OK);
	}
	
    @RequestMapping(method=RequestMethod.POST, value="/openAccount",produces=MediaType.APPLICATION_JSON_VALUE)
	public Investor openAccount(@RequestParam double cash) {
		Investor investor = context.getBean(Investor.class);
		investor.setCash(cash);
		investor.setID();
		map.put(investor.getID(), investor);
		return investor;
	}
	
    @RequestMapping(value="/closeAccount/{ID}",method=RequestMethod.DELETE)
	public ResponseEntity closeAccount(@PathVariable("ID") int ID) {
		if (!map.containsKey(ID)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		map.remove(ID);
		return new ResponseEntity<>(HttpStatus.OK);
		
	}


}
