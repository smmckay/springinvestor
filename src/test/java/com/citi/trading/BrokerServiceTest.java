package com.citi.trading;



import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.List;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;



@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@PropertySource("classpath:application.properties")
@DirtiesContext
public class BrokerServiceTest {
	
	@Configuration
	public static class Config {
		@Bean 
		public BrokerService mockBrocker() {
			BrokerService mockBroker = new BrokerService();
			return mockBroker;
		}
		
		@Bean
		@Scope("prototype")
		public Investor mockInvestor() {
			Investor investor = new Investor();
			return investor;
		}
		
		@Bean 
		public OrderPlacer mockMarket() {
			MockMarket mockMarket = new MockMarket();
			return mockMarket;
		}
	}
	
	@Autowired
	private BrokerService broker;
	
	@Test
	public void testGetAll() {
		List<Investor> investors = broker.getAll();
		assertThat(investors, hasSize(4));
	}
	@Test
	public void testgetByID() {
		Investor investor = broker.getByID(1).getBody();
		assertThat(investor.getCash(), closeTo(10000, 0.0001));
		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"), equalTo(100)));
	}
	@Test
	public void testOpenAccount() {
		Investor investor = broker.openAccount(1000);
		assertThat(investor.getCash(), closeTo(1000,0.0001));
		assertThat(investor.getID(), equalTo(5));
	}
	@Test
	public void testCloseAccount() {
		broker.closeAccount(2);
		assertThat(broker.getByID(2).getStatusCodeValue(), equalTo(404));
	}
}
